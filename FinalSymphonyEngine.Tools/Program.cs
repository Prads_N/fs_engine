﻿namespace FinalSymphonyEngine.Tools
{
    using Models.Data;
    using Nest;
    using System;
    using System.Threading.Tasks;

    class Program
    {
        static async Task Main(string[] args)
        {
            await CreateEdocsIndex();
        }

        static Task CreateEdocsIndex()
        {
            var elasticClient = new ElasticClient(new Uri("https://vpc-symphony-vxuavjjmvbfnut3k7tft66ynrm.ap-southeast-2.es.amazonaws.com"));
            return elasticClient.CreateIndexAsync("edocs", d => d.Mappings(md => md.Map<ElasticDocument>(tmd => tmd.AutoMap())));
        }

        static Task DeleteEdocsIndex()
        {
            var elasticClient = new ElasticClient(new Uri("http://localhost:9200"));
            return elasticClient.DeleteIndexAsync("edocs");
        }
    }
}
