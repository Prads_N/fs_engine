﻿namespace FinalSymphonyEngine.Utils.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtensions
    {
        public static ulong Sum(this IEnumerable<ulong> list)
        {
            var result = 0UL;

            foreach (var val in list)
                result += val;

            return result;
        }

        public static IEnumerable<IEnumerable<T>> GroupInto<T>(this IEnumerable<T> list, int size)
        {
            return list.Select((v, i) => new { Index = i, Value = v })
                .GroupBy(m => m.Index / size)
                .Select(s => s.Select(v => v.Value));
        }

        public static ICollection<T> AsCollection<T>(this IEnumerable<T> source)
        {
            return source as ICollection<T> ?? source.ToList();
        }
    }
}