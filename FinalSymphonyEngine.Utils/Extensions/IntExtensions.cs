﻿namespace FinalSymphonyEngine.Utils.Extensions
{
    using System;

    public static class IntExtensions
    {
        public static int GetOffset(this int page, int limit)
        {
            return Math.Max(page - 1, 0) * limit;
        }
    }
}