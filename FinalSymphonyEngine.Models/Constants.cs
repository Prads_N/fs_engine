﻿namespace FinalSymphonyEngine.Models
{
    public static class Constants
    {
        public static class DatabaseTables
        {
            public const string Containers = "containers";
            public const string Properties = "properties";
            public const string ContainerPropertyMappings = "container_property_mappings";
            public const string NumericValues = "numeric_values";
            public const string KeywordValues = "keyword_values";
        }

        public static class Auth
        {
            public static class Scopes
            {
                public const string Engine = "fs_engine";
            }

            public static class ApiNames
            {
                public const string Engine = "fs_engine";
            }
        }

        public static class Elasticsearch
        {
            public const int MaxDocumentWindow = 10000;
        }
    }
}