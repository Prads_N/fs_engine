﻿namespace FinalSymphonyEngine.Models
{
    public enum ValueDataType : short
    {
        Keyword = 1,
        SignedInteger = 2,
        UnsignedInteger = 3,
        Double = 4
    }
}