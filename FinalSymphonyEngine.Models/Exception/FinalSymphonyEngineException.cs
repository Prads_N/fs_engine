﻿namespace FinalSymphonyEngine.Models.Exception
{
    using System;

    public class FinalSymphonyEngineException : Exception
    {
        public FinalSymphonyEngineException(string message) : base(message) { }
    }
}