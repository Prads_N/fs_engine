﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using FinalSymphonyEngine.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPropertyRepository
    {
        Task<Property> Get(string name);
        Task<IEnumerable<Property>> GetMany(IEnumerable<string> names);
        Task Insert(string name);
    }
}