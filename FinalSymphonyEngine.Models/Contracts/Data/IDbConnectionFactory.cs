﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using System.Data;

    public interface IDbConnectionFactory
    {
        IDbConnection GetRead();
        IDbConnection GetWrite();
    }
}