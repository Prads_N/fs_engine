﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using FinalSymphonyEngine.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IContainerPropertyMappingRepository
    {
        Task Insert(IEnumerable<ContainerPropertyMapping> model);
        Task<IEnumerable<ContainerPropertyMapping>> Get(ulong containerId);
    }
}