﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IKeywordValueRepository
    {
        Task Insert(IEnumerable<KeywordValue> keywordValue);
        Task<IEnumerable<string>> GetStringValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds);
        Task<IEnumerable<string>> GetStringValues(string hashValue);
    }
}