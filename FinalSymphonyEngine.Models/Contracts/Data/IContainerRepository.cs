﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IContainerRepository
    {
        Task Create(Container container);
        Task<Container> Get(ulong ownerId, string name);
        Task<IEnumerable<Container>> GetAll(ulong ownerId);
        Task<IEnumerable<Container>> Get(ulong ownerId, int page, int limit);
        Task<Container> Get(ulong ownerId, ulong containerId);
        Task Update(Container container);
    }
}