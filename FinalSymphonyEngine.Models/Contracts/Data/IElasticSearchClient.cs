﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using FinalSymphonyEngine.Models.Data;
    using Nest;
    using Query;
    using Responses;
    using System.Threading.Tasks;

    public interface IElasticSearchClient
    {
        Task<QueryResponse> Count(ElasticQuery query);
        Task<QueryResponse> Extract(ElasticQuery query);
        Task Store(string indexName, ElasticDocument document);
        Task<ISearchResponse<ElasticDocument>> Scan(ElasticQuery query);
        Task<ISearchResponse<ElasticDocument>> Scroll(string scrollId);
    }
}