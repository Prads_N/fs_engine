﻿namespace FinalSymphonyEngine.Models.Contracts.Data
{
    using FinalSymphonyEngine.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface INumericValueRepository
    {
        Task Insert(IEnumerable<NumericValue> numericValue);
        Task<IEnumerable<double>> GetDoubleValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds);
        Task<IEnumerable<long>> GetSignedIntegerValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds);
        Task<IEnumerable<ulong>> GetUnsignedIntegerValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds);
    }
}