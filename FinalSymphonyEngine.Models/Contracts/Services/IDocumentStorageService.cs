﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using FinalSymphonyEngine.Models.Storage;
    using System.Threading.Tasks;

    public interface IDocumentStorageService
    {
        Task Store(DataStorageRequest request);
    }
}