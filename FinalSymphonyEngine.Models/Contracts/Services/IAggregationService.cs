﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using Query;
    using Query.Aggregation;
    using Responses;
    using System.Threading.Tasks;

    public interface IAggregationService
    {
        Task<QueryResponse> ExecuteAggregation(ulong containerId, ElasticQuery elasticQuery, AggregationRequest aggregationRequest);
    }
}