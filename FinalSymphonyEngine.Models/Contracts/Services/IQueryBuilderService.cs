﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using FinalSymphonyEngine.Models.Query;
    using System.Threading.Tasks;

    public interface IQueryBuilderService<TQuery>
    {
        Task<TQuery> BuildQuery(QueryRequest request);
    }
}