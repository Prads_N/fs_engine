﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using FinalSymphonyEngine.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPropertyService
    {
        Task<Property> GetOrInsert(string property);
        Task<IEnumerable<Property>> GetMany(IEnumerable<string> names);
    }
}