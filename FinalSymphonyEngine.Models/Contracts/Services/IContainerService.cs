﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using Models.Data;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IContainerService
    {
        Task<Container> GetOrInsert(ulong ownerId, string name, Func<Task<IEnumerable<ContainerPropertyMapping>>> mappingFunc);
        Task<IEnumerable<Container>> GetAll(ulong ownerId);
        Task<IEnumerable<Container>> Get(ulong ownerId, int page, int limit);
        Task<Container> Get(ulong ownerId, ulong containerId);
        Task<Container> Get(ulong ownerId, string name);
        Task Update(ulong id, string newName);
        
    }
}