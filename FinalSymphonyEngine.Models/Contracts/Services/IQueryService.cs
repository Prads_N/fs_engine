﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    using Responses;
    using Query;
    using System.Threading.Tasks;

    public interface IQueryService
    {
        Task<QueryResponse> Query(QueryRequest request);
    }
}