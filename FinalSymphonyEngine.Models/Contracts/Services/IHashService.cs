﻿namespace FinalSymphonyEngine.Models.Contracts.Services
{
    public interface IHashService
    {
        byte[] HashString(string plainText, bool caseSensitive = false);
        string HashStringToHex(string plainText, bool caseSensitive = false);
    }
}