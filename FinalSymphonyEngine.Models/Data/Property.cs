﻿namespace FinalSymphonyEngine.Models.Data
{
    public class Property
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
    }
}
