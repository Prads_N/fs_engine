﻿namespace FinalSymphonyEngine.Models.Data
{
    public class ContainerPropertyMapping
    {
        public ulong ContainerId { get; set; }
        public ulong PropertyId { get; set; }
        public ValueDataType ValueDataType { get; set; }
    }
}