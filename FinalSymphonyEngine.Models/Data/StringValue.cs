﻿namespace FinalSymphonyEngine.Models.Data
{
    public class KeywordValue
    {
        public ulong ContainerId { get; set; }
        public ulong DocumentId { get; set; }
        public ulong PropertyId { get; set; }

        public string Hash { get; set; }
        public string Keyword { get; set; }
    }
}