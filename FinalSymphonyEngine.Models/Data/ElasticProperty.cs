﻿using Nest;

namespace FinalSymphonyEngine.Models.Data
{
    public class ElasticProperty
    {
        public ulong PropertyId { get; set; }
        public long? SignedInteger { get; set; }
        public ulong? UnsignedInteger { get; set; }
        public double? Double { get; set; }
        [Keyword]
        public string Keyword { get; set; }

        [Ignore]
        public ValueDataType DataType { get; set; } 
    }
}