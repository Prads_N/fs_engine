﻿namespace FinalSymphonyEngine.Models.Data
{
    using Nest;
    using System.Collections.Generic;

    public class ElasticDocument
    {
        public ulong ContainerId { get; set; }
        public ulong DocumentId { get; set; }

        [Nested(IncludeInParent = false)]
        public IEnumerable<ElasticProperty> Properties { get; set; }
    }
}