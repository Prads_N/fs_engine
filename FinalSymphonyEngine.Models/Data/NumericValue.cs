﻿namespace FinalSymphonyEngine.Models.Data
{
    public class NumericValue
    {
        public ulong ContainerId { get; set; }
        public ulong DocumentId { get; set; }
        public ulong PropertyId { get; set; }

        public double? Double { get; set; }
        public long? SignedInteger { get; set; }
        public ulong? UnsignedInteger { get; set; }
    }
}