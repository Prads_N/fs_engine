﻿namespace FinalSymphonyEngine.Models.Query
{
    using FinalSymphonyEngine.Models.Data;
    using Nest;

    public class ElasticQuery
    {
        public SearchDescriptor<ElasticDocument> SearchDescriptor { get; set; }
        public CountDescriptor<ElasticDocument> CountDescriptor { get; set; }
    }
}