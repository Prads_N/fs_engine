﻿namespace FinalSymphonyEngine.Models.Query
{
    using System.Collections.Generic;

    public class SimpleQuery
    {
        public IEnumerable<QueryElement> Must { get; set; }
        public IEnumerable<QueryElement> Should { get; set; }
    }
}