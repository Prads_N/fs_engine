﻿namespace FinalSymphonyEngine.Models.Query.Aggregation
{
    using System.Collections.Generic;

    public class AggregationRequest
    {
        public IEnumerable<AggregationElement> AggregationElements { get; set; }
    }
}