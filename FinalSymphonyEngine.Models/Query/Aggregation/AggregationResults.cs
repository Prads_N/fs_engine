﻿namespace FinalSymphonyEngine.Models.Query.Aggregation
{
    public class AggregationResult
    {
        public object Sum { get; set; }
        public object SquaredSum { get; set; }
        public object Min { get; set; }
        public object Max { get; set; }

        public AggregationResult(object initialSum, object initialSquaredSum, object initialMin, object initialMax)
        {
            this.Sum = initialSum;
            this.SquaredSum = initialSquaredSum;
            this.Min = initialMin;
            this.Max = initialMax;
        }
    }
}