﻿namespace FinalSymphonyEngine.Models.Query.Aggregation
{
    public class AggregationElement
    {
        public string Alias { get; set; }
        public string Property { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public AggregationType AggregationType { get; set; }

        //Populated by the service
        public ulong PropertyId { get; set; }
        public AggregationResult Result { get; set; }
    }
}