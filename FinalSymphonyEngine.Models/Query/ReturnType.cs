﻿namespace FinalSymphonyEngine.Models.Query
{
    public enum ReturnType
    {
        Count = 1,
        Extraction = 2,
        Aggregation = 3
    }
}
