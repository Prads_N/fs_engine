﻿namespace FinalSymphonyEngine.Models.Query
{
    public class QueryElement
    {
        public string Property { get; set; }
        public QueryOperator Operator { get; set; }
        public string Value { get; set; }
        public ValueDataType ValueDataType { get; set; }
    }
}