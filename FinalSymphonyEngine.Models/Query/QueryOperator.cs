﻿namespace FinalSymphonyEngine.Models.Query
{
    public enum QueryOperator
    {
        Equals = 1,
        NotEqual = 2,
        GreaterThanOrEqual = 3,
        LessThanOrEqual = 4,
        Greater = 5,
        Less = 6
    }
}