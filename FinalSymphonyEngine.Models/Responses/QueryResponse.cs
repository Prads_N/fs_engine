﻿namespace FinalSymphonyEngine.Models.Responses
{
    public class QueryResponse
    {
        public object Data { get; set; }
    }
}