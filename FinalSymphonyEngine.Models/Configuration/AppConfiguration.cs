﻿namespace FinalSymphonyEngine.Models.Configuration
{
    public class AppConfiguration
    {
        public DbConnectionStrings DbConnectionStrings { get; set; }
        public ApiEndpoints ApiEndpoints { get; set; }
    }
}