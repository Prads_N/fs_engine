﻿namespace FinalSymphonyEngine.Models.Configuration
{
    public class DbConnectionStrings
    {
        public string Read { get; set; }
        public string Write { get; set; }
    }
}