﻿namespace FinalSymphonyEngine.Models.Configuration
{
    public class ApiEndpoints
    {
        public string Auth { get; set; }
        public string Elasticsearch { get; set; }
    }
}