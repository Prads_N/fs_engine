﻿namespace FinalSymphonyEngine.Models.Storage
{
    using System.Collections.Generic;

    public class DataStorageRequest
    {
        public IEnumerable<PropertyStorage> Properties { get; set; }

        public ulong? ContainerId { get; set; }
        public string ContainerName { get; set; }

        public ulong OwnerId { get; set; }
        public ulong DocumentId { get; set; }
        public string Index { get; set; }
    }
}