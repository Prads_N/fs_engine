﻿namespace FinalSymphonyEngine.Models.Storage
{
    public class PropertyStorage
    {
        public string PropertyName { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public object Value { get; set; }
    }
}
