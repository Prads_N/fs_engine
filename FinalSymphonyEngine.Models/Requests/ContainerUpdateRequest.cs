﻿namespace FinalSymphonyEngine.Models.Requests
{
    public class ContainerUpdateRequest
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
    }
}