﻿namespace FinalSymphonyEngine.Models.Requests
{
    using System.Collections.Generic;

    public class ContainerMappingRefreshRequest
    {
        public string ContainerName { get; set; }
        public IEnumerable<ContainerMappingRequestProperty> Properties { get; set; }
    }

    public class ContainerMappingRequestProperty
    {
        public string PropertyName { get; set; }
        public ValueDataType ValueDataType { get; set; }
    }
}