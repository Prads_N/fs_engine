CREATE DATABASE IF NOT EXISTS `final_symphony_engine`;
USE `final_symphony_engine`;

CREATE TABLE `containers` (
	`Id`	BIGINT UNSIGNED AUTO_INCREMENT,
	`Name`	VARCHAR(256) NOT NULL,
	`OwnerId` BIGINT UNSIGNED NOT NULL,
	
	`DateCreated`	DATETIME NOT NULL,
	`DateModified`	DATETIME DEFAULT NULL,
	
	PRIMARY KEY (`Id`),
	UNIQUE INDEX `idx_ownerid_name` (`OwnerId`, `Name`)
 )engine=innodb default charset=utf8mb4;
 
 CREATE TABLE `properties` (
	`Id`		BIGINT UNSIGNED AUTO_INCREMENT,
	`Name`		VARCHAR(256) NOT NULL,
	
	PRIMARY KEY (`Id`),
	UNIQUE INDEX `idx_name` (`Name`)
 )engine=innodb default charset=utf8mb4;
 
 CREATE TABLE `container_property_mappings` (
	`ContainerId`		BIGINT UNSIGNED NOT NULL,
	`PropertyId`		BIGINT UNSIGNED NOT NULL,
	`ValueDataType`		SMALLINT NOT NULL,
	
	PRIMARY KEY (`ContainerId`,`PropertyId`,`ValueDataType`)
 )engine=innodb default charset=utf8mb4;
 
 CREATE TABLE `NumericValues` (
	`ContainerId`			BIGINT UNSIGNED NOT NULL,
	`DocumentId`			BIGINT UNSIGNED NOT NULL,
	`PropertyId`			BIGINT UNSIGNED NOT NULL,
	
	`DoubleValue`			DOUBLE,
	`SignedIntegerValue`	BIGINT,
	`UnsignedIntegerValue`	BIGINT UNSIGNED,
	
	PRIMARY KEY (`ContainerId`,`PropertyId`,`DocumentId`)
 )engine=innodb default charset=utf8mb4;
 
 CREATE TABLE `KeywordValues` (
	`ContainerId`			BIGINT UNSIGNED NOT NULL,
	`DocumentId`			BIGINT UNSIGNED NOT NULL,
	`PropertyId`			BIGINT UNSIGNED NOT NULL,
	
	`HashValue`				CHAR(64),
	`StringValue`			LONGTEXT,
	
	PRIMARY KEY (`ContainerId`,`PropertyId`,`DocumentId`),
	INDEX idx_stringvalues_hashvalue (`HashValue`)
 )engine=innodb default charset=utf8mb4;