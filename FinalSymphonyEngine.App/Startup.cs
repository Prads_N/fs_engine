﻿namespace FinalSymphonyEngine.App
{
    using Models.Contracts.Services;
    using Services;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Models.Contracts.Data;
    using Data.Repositories;
    using Models.Query;
    using Data;
    using Models.Configuration;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using FinalSymphonyEngine.Models;
    using Microsoft.AspNetCore.Authorization;
    using FinalSymphonyEngine.Data.Elastic;
    using FinalSymphonyEngine.App.Middlewares;

    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var config = _configuration.Get<AppConfiguration>();

            services.AddMvcCore().AddAuthorization().AddJsonFormatters();
            AddAuth(services, config);

            #region Repositories

            services.AddSingleton<IElasticSearchClient>(new ElasticSearchClient(config.ApiEndpoints.Elasticsearch));

            services.AddSingleton<IContainerRepository, ContainerRepository>();
            services.AddSingleton<IPropertyRepository, PropertyRepository>();
            services.AddSingleton<IContainerPropertyMappingRepository, ContainerPropertyMappingRepository>();
            services.AddSingleton<INumericValueRepository, NumericValueRepository>();
            services.AddSingleton<IKeywordValueRepository, KeywordValueRepository>();

            #endregion

            #region Services

            services.AddSingleton<IContainerService, ContainerService>();
            services.AddSingleton<IDocumentStorageService, DocumentStorageService>();
            services.AddSingleton<IHashService, HashService>();
            services.AddSingleton<IPropertyService, PropertyService>();
            services.AddSingleton<IQueryBuilderService<ElasticQuery>, ElasticQueryBuilderService>();
            services.AddSingleton<IQueryService, QueryService>();
            services.AddSingleton<IAggregationService, AggregationService>();

            #endregion

            #region DB Connection

            services.AddSingleton<IDbConnectionFactory>(new DbConnectionFactory(config.DbConnectionStrings.Read, config.DbConnectionStrings.Write));

            #endregion
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMvc();
        }

        private void AddAuth(IServiceCollection services, AppConfiguration config)
        {
            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddIdentityServerAuthentication(o =>
            {
                o.Authority = config.ApiEndpoints.Auth;
                o.ApiName = Constants.Auth.ApiNames.Engine;
            });

            services.AddAuthorization(o =>
            {
                o.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .RequireScope(Constants.Auth.Scopes.Engine)
                    .Build();
            });
        }
    }
}