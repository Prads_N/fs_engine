﻿namespace FinalSymphonyEngine.App.Controller
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models.Contracts.Services;
    using Primitives;
    using System.Threading.Tasks;

    [Route("v1/owners"), Authorize]
    public class OwnerController : BaseController
    {
        private readonly IContainerService _containerService;

        public OwnerController(IContainerService containerService)
        {
            _containerService = containerService;
        }

        [HttpGet("{ownerId}/containers")]
        public async Task<IActionResult> GetContainers(ulong ownerId, int page = 1, int limit = 25)
        {
            return Ok(await _containerService.Get(ownerId, page, limit));
        }

        [HttpGet("{ownerId}/containers/{containerId}")]
        public async Task<IActionResult> GetContainer(ulong ownerId, ulong containerId)
        {
            return Ok(await _containerService.Get(ownerId, containerId));
        }
    }
}