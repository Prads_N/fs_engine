﻿namespace FinalSymphonyEngine.App.Controller
{
    using Primitives;
    using Microsoft.AspNetCore.Mvc;
    using Models.Contracts.Services;
    using System.Threading.Tasks;
    using Models.Storage;
    using Microsoft.AspNetCore.Authorization;

    [Route("v1/documents"), Authorize]
    public class DocumentController : BaseController
    {
        private readonly IDocumentStorageService _documentStorageService;

        public DocumentController(IDocumentStorageService documentStorageService)
        {
            _documentStorageService = documentStorageService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Store([FromBody]DataStorageRequest request)
        {
            await _documentStorageService.Store(request);
            return Ok();
        }
    }
}