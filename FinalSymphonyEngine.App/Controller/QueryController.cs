﻿namespace FinalSymphonyEngine.App.Controller
{
    using FinalSymphonyEngine.Models.Query;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models.Contracts.Services;
    using Primitives;
    using System.Threading.Tasks;

    [Route("v1/query"), Authorize]
    public class QueryController : BaseController
    {
        private readonly IQueryService _queryService;

        public QueryController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Query([FromBody]QueryRequest queryResult)
        {
            return Ok(await _queryService.Query(queryResult));
        }
    }
}