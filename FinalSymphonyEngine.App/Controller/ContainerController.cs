﻿namespace FinalSymphonyEngine.App.Controller
{
    using FinalSymphonyEngine.Models.Data;
    using FinalSymphonyEngine.Models.Requests;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Models.Contracts.Services;
    using Primitives;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Route("v1/containers"), Authorize]
    public class ContainerController : BaseController
    {
        private readonly IContainerService _containerService;

        public ContainerController(IContainerService containerService)
        {
            _containerService = containerService;
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] ContainerUpdateRequest request)
        {
            await _containerService.Update(request.Id, request.Name);
            return Ok();
        }

        [HttpPost("mappings")]
        public async Task<IActionResult> RefreshMappings(ContainerMappingRefreshRequest request)
        {
            throw new NotImplementedException();
        }
    }
}