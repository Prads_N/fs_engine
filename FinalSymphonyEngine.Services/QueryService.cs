﻿namespace FinalSymphonyEngine.Services
{
    using System.Threading.Tasks;
    using Models.Contracts.Data;
    using Models.Query;
    using Models.Responses;
    using Models.Contracts.Services;
    using Models.Exception;

    public class QueryService : IQueryService
    {
        private readonly IQueryBuilderService<ElasticQuery> _queryBuilder;
        private readonly IContainerService _containerService;
        private readonly IElasticSearchClient _elasticSearchClient;
        private readonly IAggregationService _aggregationService;

        public QueryService(IQueryBuilderService<ElasticQuery> queryBuilder, IContainerService containerService, 
            IElasticSearchClient elasticSearchClient, IAggregationService aggregationService)
        {
            _queryBuilder = queryBuilder;
            _containerService = containerService;
            _elasticSearchClient = elasticSearchClient;
            _aggregationService = aggregationService;
        }

        public async Task<QueryResponse> Query(QueryRequest request)
        {
            if (!request.ContainerId.HasValue)
            {
                if (string.IsNullOrWhiteSpace(request.ContainerName))
                    throw new FinalSymphonyEngineException("Container information is missing");

                var container = await _containerService.Get(request.OwnerId, request.ContainerName);

                if (container == null)
                    return new QueryResponse { Data = null };

                request.ContainerId = container.Id;
            }

            var query = await _queryBuilder.BuildQuery(request);

            switch (request.ReturnType)
            {
                case ReturnType.Aggregation:
                    return await _aggregationService.ExecuteAggregation(request.ContainerId.Value, query, request.AggregationRequest);
                case ReturnType.Count:
                    return await _elasticSearchClient.Count(query);
                default:
                    return await _elasticSearchClient.Extract(query);
            }
        }
    }
}