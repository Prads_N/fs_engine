﻿namespace FinalSymphonyEngine.Services
{
    using Models.Data;
    using Models.Contracts.Data;
    using System.Threading.Tasks;
    using FinalSymphonyEngine.Models.Contracts.Services;
    using System.Collections.Generic;

    public class PropertyService : IPropertyService
    {
        private readonly IPropertyRepository _propertyRepository;

        public PropertyService(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }

        public async Task<Property> GetOrInsert(string property)
        {
            property = property.Trim();

            var model = await _propertyRepository.Get(property);

            if (model == null)
                await _propertyRepository.Insert(property);
            else
                return model;

            return await _propertyRepository.Get(property);
        }

        public Task<IEnumerable<Property>> GetMany(IEnumerable<string> names)
        {
            return _propertyRepository.GetMany(names);
        }
    }
}