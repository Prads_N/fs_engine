﻿namespace FinalSymphonyEngine.Services
{
    using Models.Contracts.Services;
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public class HashService : IHashService
    {
        public byte[] HashString(string plainText, bool caseSensitive = false)
        {
            if (String.IsNullOrWhiteSpace(plainText))
                return null;

            using (var sha256 = SHA256.Create())
                return sha256.ComputeHash(Encoding.ASCII.GetBytes(caseSensitive ? plainText : plainText.ToLower()));
        }

        public string HashStringToHex(string plainText, bool caseSensitive = false)
        {
            if (String.IsNullOrWhiteSpace(plainText))
                return null;

            using (var sha256 = SHA256.Create())
            {
                return BitConverter.ToString(sha256.ComputeHash(Encoding.ASCII.GetBytes(caseSensitive ? plainText : plainText.ToLower())))
                    .Replace("-", "").ToLower();
            }
        }
    }
}