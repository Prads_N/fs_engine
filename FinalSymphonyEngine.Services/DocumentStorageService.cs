﻿namespace FinalSymphonyEngine.Services
{
    using FinalSymphonyEngine.Models;
    using FinalSymphonyEngine.Models.Contracts.Data;
    using FinalSymphonyEngine.Models.Data;
    using FinalSymphonyEngine.Models.Exception;
    using FinalSymphonyEngine.Utils.Extensions;
    using Models.Contracts.Services;
    using Models.Storage;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DocumentStorageService : IDocumentStorageService
    {
        private readonly IHashService _hashService;
        private readonly IPropertyService _propertyService;
        private readonly IContainerService _containerService;
        private readonly IElasticSearchClient _elasticSearchClient;
        private readonly INumericValueRepository _numericValueRepository;
        private readonly IKeywordValueRepository _keywordValueRepository;

        public DocumentStorageService(IHashService hashService, IPropertyService propertyService,
            IContainerService containerService, IElasticSearchClient elasticSearchClient, IKeywordValueRepository keywordValueRepository)
        {
            _hashService = hashService;
            _propertyService = propertyService;
            _containerService = containerService;
            _elasticSearchClient = elasticSearchClient;
            _keywordValueRepository = keywordValueRepository;
        }

        public async Task Store(DataStorageRequest request)
        {
            if (!request.ContainerId.HasValue)
            {
                if (String.IsNullOrWhiteSpace(request.ContainerName))
                    throw new FinalSymphonyEngineException("Both Container Id and Name cannot be null. You need to provide one or the other.");

                var container = await _containerService.GetOrInsert(request.OwnerId, request.ContainerName, () => GetContainerPropertyMappings(request));

                if (container == null)
                    throw new FinalSymphonyEngineException($"Couldn't get container with name {request.ContainerName}");

                request.ContainerId = container.Id;
            }

            var elasticDocument = new ElasticDocument
            {
                ContainerId = request.ContainerId.Value,
                DocumentId = request.DocumentId,
                Properties = await this.GetElasticProperties(request)
            };

            if (!elasticDocument.Properties.Any())
                throw new FinalSymphonyEngineException("Document doesn't contain any properties");

            //Store each properties separately in the database first before sending it to elasticsearch
            await StorePropertiesToDb(elasticDocument);

            await _elasticSearchClient.Store(request.Index, elasticDocument);
        }

        private async Task StorePropertiesToDb(ElasticDocument elasticDocument)
        {
            //Separate numeric and non numeric ones
            var numericProperties = elasticDocument.Properties.Where(p => p.DataType != ValueDataType.Keyword);
            var keywordProperties = elasticDocument.Properties.Where(p => p.DataType == ValueDataType.Keyword);

            var numericValues = numericProperties.Select(property =>
            {
                var numericValue = property.Convert<NumericValue>();

                numericValue.ContainerId = elasticDocument.ContainerId;
                numericValue.DocumentId = elasticDocument.DocumentId;

                return numericValue;
            });

            var keywordValues = keywordProperties.Select(property =>
            {
                var keywordValue = property.Convert<KeywordValue>();

                keywordValue.ContainerId = elasticDocument.ContainerId;
                keywordValue.DocumentId = elasticDocument.DocumentId;

                return keywordValue;
            });

            var tasks = new List<Task>(2);

            if (numericValues.Any())
                tasks.Add(_numericValueRepository.Insert(numericValues));

            if (keywordValues.Any())
                tasks.Add(_keywordValueRepository.Insert(keywordValues));

            await Task.WhenAll(tasks);
        }

        private async Task<IEnumerable<ElasticProperty>> GetElasticProperties(DataStorageRequest request)
        {
            var properties = new List<ElasticProperty>();

            foreach (var property in request.Properties)
            {
                var storedProperty = await _propertyService.GetOrInsert(property.PropertyName);

                var elasticProperty = new ElasticProperty
                {
                    PropertyId = storedProperty.Id,
                    DataType = property.ValueDataType
                };

                switch (property.ValueDataType)
                {
                    case ValueDataType.Double:
                        elasticProperty.Double = property.Value == null ? null : (double?)Double.Parse(property.Value.ToString());
                        break;
                    case ValueDataType.Keyword:
                        elasticProperty.Keyword = property.Value == null ? null : _hashService.HashStringToHex(property.Value.ToString());
                        break;
                    case ValueDataType.SignedInteger:
                        elasticProperty.SignedInteger = property.Value == null ? null : (long?)Int64.Parse(property.Value.ToString());
                        break;
                    case ValueDataType.UnsignedInteger:
                        elasticProperty.UnsignedInteger = property.Value == null ? null : (ulong?)UInt64.Parse(property.Value.ToString());
                        break;
                    default:
                        throw new FinalSymphonyEngineException($"Unknown value data type {property.ValueDataType}");
                }

                properties.Add(elasticProperty);
            }

            return properties;
        }

        private async Task<IEnumerable<ContainerPropertyMapping>> GetContainerPropertyMappings(DataStorageRequest request)
        {
            return await Task.WhenAll(request.Properties.Select(async property => new ContainerPropertyMapping
            {
                ContainerId = request.ContainerId.HasValue ? request.ContainerId.Value : 0,
                PropertyId = (await _propertyService.GetOrInsert(property.PropertyName)).Id,
                ValueDataType = property.ValueDataType
            }));
        }
    }
}