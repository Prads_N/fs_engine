﻿namespace FinalSymphonyEngine.Services
{
    using System.Threading.Tasks;
    using Models.Query;
    using Models.Contracts.Services;
    using Models.Data;
    using Nest;
    using Models.Exception;
    using Utils.Extensions;
    using System.Linq;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public class ElasticQueryBuilderService : IQueryBuilderService<ElasticQuery>
    {
        private readonly IHashService _hashService;
        private readonly IPropertyService _propertyService;
        private readonly Dictionary<ValueDataType, Func<QueryElement, QueryContainer>> _queryElementToQueryContainer;

        public ElasticQueryBuilderService(IHashService hashService, IPropertyService propertyService)
        {
            _hashService = hashService;
            _propertyService = propertyService;

            _queryElementToQueryContainer = new Dictionary<ValueDataType, Func<QueryElement, QueryContainer>>()
            {
                { ValueDataType.Double, this.GetDoubleQuery },
                { ValueDataType.Keyword, this.GetKeywordQuery },
                { ValueDataType.SignedInteger, this.GetSignedIntegerQuery },
                { ValueDataType.UnsignedInteger, this.GetUnsignedIntegerQuery }
            };
        }
 
        public Task<ElasticQuery> BuildQuery(QueryRequest request)
        {
            switch (request.QueryType)
            {
                case QueryType.SIMPLE:
                    return this.GetSimpleQuery(request);
                default:
                    throw new FinalSymphonyEngineException("Unknown query type");
            }
        }

        public async Task<ElasticQuery> GetSimpleQuery(QueryRequest request)
        {
            var simpleQuery = request.QueryData.Convert<SimpleQuery>();
            var propertyDictionary = await this.GetPropertyDictionary(simpleQuery);

            QueryContainer queryContainer = Query<ElasticDocument>.Term(t => t.ContainerId, request.ContainerId);

            if (simpleQuery.Must != null && simpleQuery.Must.Any())
            {
                foreach (var must in simpleQuery.Must)
                {
                    queryContainer = queryContainer && Query<ElasticDocument>
                    .Nested(s => s.Path(p => p.Properties)
                    .Query(qs => this.BuildQueryElementQuery(must, propertyDictionary)));
                }
            }

            if (simpleQuery.Should != null && simpleQuery.Should.Any())
            {
                var shouldContainer = new QueryContainer();

                foreach (var should in simpleQuery.Should)
                {
                    shouldContainer = shouldContainer || Query<ElasticDocument>
                        .Nested(s => s.Path(p => p.Properties)
                        .Query(q => this.BuildQueryElementQuery(should, propertyDictionary)));
                }

                queryContainer = queryContainer && shouldContainer;
            }

            var result = new ElasticQuery();

            if (request.ReturnType == ReturnType.Count)
            {
                result.CountDescriptor = new CountDescriptor<ElasticDocument>()
                    .Index(request.Index).Type(nameof(ElasticDocument).ToLower()).Query(qd => queryContainer);
            }
            else
            {
                result.SearchDescriptor = new SearchDescriptor<ElasticDocument>()
                    .Index(request.Index).Type(nameof(ElasticDocument).ToLower())
                    .Query(qd => queryContainer)
                    .Source(s => s.Includes(i => i.Field(f => f.DocumentId)))
                    .Size(Constants.Elasticsearch.MaxDocumentWindow);
            }
            
            return result;
        }

        private QueryContainer BuildQueryElementQuery(QueryElement queryElement, Dictionary<string, Property> propertyDictionary)
        {
            var property = propertyDictionary[queryElement.Property];
            var queryFunc = _queryElementToQueryContainer[queryElement.ValueDataType];

            return Query<ElasticDocument>.Bool(bs =>
                bs.Must(m => m.Term(t => t.Properties.First().PropertyId, property.Id),
                        m => queryFunc(queryElement)));
        }

        private QueryContainer GetKeywordQuery(QueryElement queryElement)
        {
            if (String.IsNullOrWhiteSpace(queryElement.Value))
                return GetNullQuery(queryElement, f => f.Properties.First().Keyword);

            var value = this.GetQueryElementValue(queryElement);

            if (queryElement.Operator == QueryOperator.NotEqual)
                return Query<ElasticDocument>.Bool(bs => bs.MustNot(mn => mn.Term(t => t.Properties.First().Keyword, value)));

            return Query<ElasticDocument>.Term(t => t.Properties.First().Keyword, value);
        }

        private QueryContainer GetSignedIntegerQuery(QueryElement queryElement)
        {
            if (String.IsNullOrWhiteSpace(queryElement.Value))
                return GetNullQuery(queryElement, f => f.Properties.First().SignedInteger);

            var value = this.GetQueryElementValue(queryElement);

            switch (queryElement.Operator)
            {
                case QueryOperator.GreaterThanOrEqual:
                    return Query<ElasticDocument>.LongRange(r => r.Field(f => f.Properties.First().SignedInteger).GreaterThanOrEquals((long)value));
                case QueryOperator.LessThanOrEqual:
                    return Query<ElasticDocument>.LongRange(r => r.Field(f => f.Properties.First().SignedInteger).LessThanOrEquals((long)value));
                case QueryOperator.Greater:
                    return Query<ElasticDocument>.LongRange(r => r.Field(f => f.Properties.First().SignedInteger).GreaterThan((long)value));
                case QueryOperator.Less:
                    return Query<ElasticDocument>.LongRange(r => r.Field(f => f.Properties.First().SignedInteger).LessThan((long)value));
                case QueryOperator.NotEqual:
                    return Query<ElasticDocument>.Bool(bs => bs.MustNot(mn => mn.Term(t => t.Properties.First().SignedInteger, value)));
                default:
                    return Query<ElasticDocument>.Term(t => t.Properties.First().SignedInteger, value);
            }
        }

        private QueryContainer GetUnsignedIntegerQuery(QueryElement queryElement)
        {
            if (String.IsNullOrWhiteSpace(queryElement.Value))
                return GetNullQuery(queryElement, f => f.Properties.First().UnsignedInteger);

            var value = this.GetQueryElementValue(queryElement);

            switch (queryElement.Operator)
            {
                case QueryOperator.GreaterThanOrEqual:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().UnsignedInteger).GreaterThanOrEquals(Convert.ToDouble(value)));
                case QueryOperator.LessThanOrEqual:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().UnsignedInteger).LessThanOrEquals(Convert.ToDouble(value)));
                case QueryOperator.Greater:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().UnsignedInteger).GreaterThan(Convert.ToDouble(value)));
                case QueryOperator.Less:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().UnsignedInteger).LessThan(Convert.ToDouble(value)));
                case QueryOperator.NotEqual:
                    return Query<ElasticDocument>.Bool(bs => bs.MustNot(mn => mn.Term(t => t.Properties.First().UnsignedInteger, value)));
                default:
                    return Query<ElasticDocument>.Term(t => t.Properties.First().UnsignedInteger, value);
            }
        }

        private QueryContainer GetDoubleQuery(QueryElement queryElement)
        {
            if (String.IsNullOrWhiteSpace(queryElement.Value))
                return GetNullQuery(queryElement, f => f.Properties.First().Double);

            var value = this.GetQueryElementValue(queryElement);

            switch (queryElement.Operator)
            {
                case QueryOperator.GreaterThanOrEqual:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().Double).GreaterThanOrEquals((double)value));
                case QueryOperator.LessThanOrEqual:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().Double).LessThanOrEquals((double)value));
                case QueryOperator.Greater:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().Double).GreaterThan((double)value));
                case QueryOperator.Less:
                    return Query<ElasticDocument>.Range(r => r.Field(f => f.Properties.First().Double).LessThan((double)value));
                case QueryOperator.NotEqual:
                    return Query<ElasticDocument>.Bool(bs => bs.MustNot(mn => mn.Term(t => t.Properties.First().Double, value)));
                default:
                    return Query<ElasticDocument>.Term(t => t.Properties.First().Double, value);
            }
        }

        private QueryContainer GetNullQuery(QueryElement queryElement, Expression<Func<ElasticDocument, object>> field)
        {
            if (queryElement.Operator == QueryOperator.Equals)
                return Query<ElasticDocument>.Bool(bs => bs.MustNot(mn => mn.Exists(s => s.Field(field))));

            return Query<ElasticDocument>.Exists(s => s.Field(field));
        }

        private async Task<Dictionary<string, Property>> GetPropertyDictionary(SimpleQuery simpleQuery)
        {
            var properties = new List<string>();

            if (simpleQuery.Must != null)
                properties.AddRange(simpleQuery.Must.Select(q => q.Property));

            if (simpleQuery.Should != null)
                properties.AddRange(simpleQuery.Should.Select(q => q.Property));

            properties = properties.Distinct().ToList();

            if (!properties.Any())
                throw new FinalSymphonyEngineException("Must contain a filter");

            return (await _propertyService.GetMany(properties)).ToDictionary(ks => ks.Name, StringComparer.OrdinalIgnoreCase);
        }

        private object GetQueryElementValue(QueryElement queryElement)
        {
            switch (queryElement.ValueDataType)
            {
                case ValueDataType.Double:
                    return Double.Parse(queryElement.Value);
                case ValueDataType.Keyword:
                    return _hashService.HashStringToHex(queryElement.Value);
                case ValueDataType.SignedInteger:
                    return Int64.Parse(queryElement.Value);
                case ValueDataType.UnsignedInteger:
                    return UInt64.Parse(queryElement.Value);
                default:
                    throw new FinalSymphonyEngineException($"Unknown data type {queryElement.ValueDataType}");
            }
        }
    }
}