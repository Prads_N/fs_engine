﻿namespace FinalSymphonyEngine.Services
{
    using Models;
    using Utils.Extensions;
    using Models.Contracts.Data;
    using Models.Contracts.Services;
    using Models.Exception;
    using Models.Query;
    using Models.Query.Aggregation;
    using Models.Responses;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AggregationService : IAggregationService
    {
        private readonly IPropertyService _propertyService;
        private readonly INumericValueRepository _numericValueRepository;
        private readonly IElasticSearchClient _elasticSearchClient;

        public AggregationService(IPropertyService propertyService, INumericValueRepository numericValueRepository,
            IElasticSearchClient elasticSearchClient)
        {
            _propertyService = propertyService;
            _numericValueRepository = numericValueRepository;
            _elasticSearchClient = elasticSearchClient;
        }

        public async Task<QueryResponse> ExecuteAggregation(ulong containerId, ElasticQuery elasticQuery, AggregationRequest aggregationRequest)
        {
            this.ValidateAggregationRequest(aggregationRequest);

            var properties = (await _propertyService.GetMany(aggregationRequest.AggregationElements.Select(ar => ar.Property).Distinct()))
                .ToDictionary(ks => ks.Name);

            foreach (var aggregationElement in aggregationRequest.AggregationElements)
            {
                if (aggregationElement.ValueDataType == ValueDataType.Keyword)
                    throw new FinalSymphonyEngineException("Unsupported type for aggregation");

                if (!properties.TryGetValue(aggregationElement.Property, out var property))
                    throw new FinalSymphonyEngineException($"Couldn't find property {aggregationElement.Property}");

                aggregationElement.PropertyId = property.Id;

                switch (aggregationElement.ValueDataType)
                {
                    case ValueDataType.Double:
                        aggregationElement.Result = new AggregationResult(0.0D, 0.0D, Double.MaxValue, Double.MinValue);
                        break;
                    case ValueDataType.SignedInteger:
                        aggregationElement.Result = new AggregationResult(0L, 0L, Int64.MaxValue, Int64.MinValue);
                        break;
                    case ValueDataType.UnsignedInteger:
                        aggregationElement.Result = new AggregationResult(0UL, 0UL, UInt64.MaxValue, UInt64.MinValue);
                        break;
                    default:
                        throw new FinalSymphonyEngineException("Unknown type");
                }
            }

            var documentCount = await this.GetAggregationResults(containerId, elasticQuery, aggregationRequest.AggregationElements);
            return await this.CreateResponse(documentCount, aggregationRequest.AggregationElements);
        }

        private void ValidateAggregationRequest(AggregationRequest request)
        {
            if (request == null)
                throw new FinalSymphonyEngineException("Aggregation request is null");

            //Make sure all the aliases are unique
            var aliases = new HashSet<string>(request.AggregationElements.Select(s => s.Alias));

            if (aliases.Count != request.AggregationElements.Count())
                throw new FinalSymphonyEngineException("Aggregation alias names must be unique");
        }

        private Task<QueryResponse> CreateResponse(ulong documentCount, IEnumerable<AggregationElement> elements)
        {
            var results = new Dictionary<string, object>();

            foreach (var element in elements)
            {
                switch (element.AggregationType)
                {
                    case AggregationType.Sum:
                        results.Add(element.Alias, element.Result.Sum);
                        break;
                    case AggregationType.Mean:
                        results.Add(element.Alias, Convert.ToDouble(element.Result.Sum) / documentCount);
                        break;
                    case AggregationType.Variance:
                        results.Add(element.Alias, this.CalculateVariance(documentCount, element));
                        break;
                    case AggregationType.StandardDeviation:
                        results.Add(element.Alias, Math.Sqrt(this.CalculateVariance(documentCount, element)));
                        break;
                    case AggregationType.Min:
                        results.Add(element.Alias, element.Result.Min);
                        break;
                    case AggregationType.Max:
                        results.Add(element.Alias, element.Result.Max);
                        break;
                }
            }

            return Task.FromResult(new QueryResponse
            {
                Data = results
            });
        }

        private double CalculateVariance(ulong documentCount, AggregationElement element)
        {
            var mean = Convert.ToDouble(element.Result.Sum) / documentCount;
            return (Convert.ToDouble(element.Result.SquaredSum) / documentCount) - Math.Pow(mean, 2);
        }

        private async Task<ulong> GetAggregationResults(ulong containerId, ElasticQuery elasticQuery, IEnumerable<AggregationElement> aggregationElement)
        {
            var count = 0UL;
            string scrollId = null;
            var bufferMultiply = (int)Math.Ceiling(50000.0D / Constants.Elasticsearch.MaxDocumentWindow); //We want around 50000 ids per page
            var aggregationGroup = aggregationElement.GroupBy(e => (e.PropertyId, e.ValueDataType)).GroupInto(5);

            //Maybe this needs a timeout so we aren't retrieving too many results
            while (true)
            {
                var scanResult = await this.GetDocumentIds(scrollId, bufferMultiply, elasticQuery);
                var documentIds = scanResult.DocumentIds.AsCollection();

                if (!scanResult.DocumentIds.Any())
                    break;

                scrollId = scanResult.ScrollId;
                count += (ulong)documentIds.Count;

                foreach (var group in aggregationGroup)
                {
                    var tasks = group.Select(elements =>
                    {
                        switch (elements.Key.ValueDataType)
                        {
                            case ValueDataType.Double:
                                return ProcessDoubleResult(containerId, elements.Key.PropertyId, documentIds, elements.Select(e => e.Result));
                            case ValueDataType.SignedInteger:
                                return ProcessSignedIntegerResult(containerId, elements.Key.PropertyId, documentIds, elements.Select(e => e.Result));
                            case ValueDataType.UnsignedInteger:
                                return ProcessUnsignedIntegerResult(containerId, elements.Key.PropertyId, documentIds, elements.Select(e => e.Result));
                            default:
                                throw new FinalSymphonyEngineException("Unknown type");
                        }
                    });
                }
            }

            return count;
        }

        private async Task<ScanResult> GetDocumentIds(string scrollId, int bufferMultiply, ElasticQuery elasticQuery)
        {
            var multiplyOffset = 0;
            var documentIds = new List<ulong>();

            if (String.IsNullOrWhiteSpace(scrollId))
            {
                var response = await _elasticSearchClient.Scan(elasticQuery);

                if (!response.Documents.Any())
                    return new ScanResult(null, Enumerable.Empty<ulong>());

                scrollId = response.ScrollId;
                documentIds.AddRange(response.Documents.Select(s => s.DocumentId));
                multiplyOffset = 1;
            }

            for (int i = multiplyOffset; i < bufferMultiply; ++i)
            {
                var response = await _elasticSearchClient.Scroll(scrollId);

                if (!response.Documents.Any())
                {
                    scrollId = null;
                    break;
                }

                scrollId = response.ScrollId;
                documentIds.AddRange(response.Documents.Select(s => s.DocumentId));
            }

            return new ScanResult(scrollId, documentIds);
        }

        private async Task ProcessDoubleResult(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds, IEnumerable<AggregationResult> results)
        {
            var values = await _numericValueRepository.GetDoubleValues(containerId, propertyId, documentIds);

            var sum = values.Sum();
            var squaredSum = values.Select(v => v * v).Sum();
            var min = values.Min();
            var max = values.Max();

            foreach (var result in results)
            {
                result.Sum = (double)result.Sum + sum;
                result.SquaredSum = (double)result.SquaredSum + squaredSum;

                if ((double)result.Min > min)
                    result.Min = min;

                if ((double)result.Max < max)
                    result.Max = max;
            }
        }

        private async Task ProcessSignedIntegerResult(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds, IEnumerable<AggregationResult> results)
        {
            var values = await _numericValueRepository.GetSignedIntegerValues(containerId, propertyId, documentIds);

            var sum = values.Sum();
            var squaredSum = values.Select(v => v * v).Sum();
            var min = values.Min();
            var max = values.Max();

            foreach (var result in results)
            {
                result.Sum = (long)result.Sum + sum;
                result.SquaredSum = (long)result.SquaredSum + squaredSum;

                if ((long)result.Min > min)
                    result.Min = min;

                if ((long)result.Max < max)
                    result.Max = max;
            }
        }

        private async Task ProcessUnsignedIntegerResult(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds, IEnumerable<AggregationResult> results)
        {
            var values = await _numericValueRepository.GetUnsignedIntegerValues(containerId, propertyId, documentIds);

            var sum = values.Sum();
            var squaredSum = values.Select(v => v * v).Sum();
            var min = values.Min();
            var max = values.Max();

            foreach (var result in results)
            {
                result.Sum = (ulong)result.Sum + sum;
                result.SquaredSum = (ulong)result.SquaredSum + squaredSum;

                if ((ulong)result.Min > min)
                    result.Min = min;

                if ((ulong)result.Max < max)
                    result.Max = max;
            }
        }

        private class ScanResult
        {
            public string ScrollId { get; set; }
            public IEnumerable<ulong> DocumentIds { get; set; }

            public ScanResult(string scrollId, IEnumerable<ulong> documentIds)
            {
                this.ScrollId = scrollId;
                this.DocumentIds = documentIds;
            }
        }
    }
}