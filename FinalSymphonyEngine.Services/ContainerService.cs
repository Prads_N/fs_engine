﻿namespace FinalSymphonyEngine.Services
{
    using Models.Contracts.Services;
    using Models.Contracts.Data;
    using Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System;
    using System.Linq;
    using FinalSymphonyEngine.Models.Requests;

    public class ContainerService : IContainerService
    {
        private readonly IContainerRepository _containerRepository;
        private readonly IContainerPropertyMappingRepository _containerPropertyMappingRepository;

        public ContainerService(IContainerRepository containerRepository, IContainerPropertyMappingRepository containerPropertyMappingRepository)
        {
            _containerRepository = containerRepository;
            _containerPropertyMappingRepository = containerPropertyMappingRepository;
        }

        public async Task<Container> GetOrInsert(ulong ownerId, string name, Func<Task<IEnumerable<ContainerPropertyMapping>>> mappingFunc)
        {
            name = name.Trim();

            var container = await _containerRepository.Get(ownerId, name);

            if (container == null)
            {
                await _containerRepository.Create(new Container
                {
                    Name = name,
                    OwnerId = ownerId
                });

                container = await _containerRepository.Get(ownerId, name);
                var containerPropertyMappings = (await mappingFunc()).ToList();

                foreach (var map in containerPropertyMappings)
                    map.ContainerId = container.Id;

                await RefreshMapping(containerPropertyMappings);
            }

            return container;
        }

        public Task<IEnumerable<Container>> GetAll(ulong ownerId)
        {
            return _containerRepository.GetAll(ownerId);
        }

        public Task<IEnumerable<Container>> Get(ulong ownerId, int page, int limit)
        {
            return _containerRepository.Get(ownerId, page, limit);
        }

        public Task<Container> Get(ulong ownerId, ulong containerId)
        {
            return _containerRepository.Get(ownerId, containerId);
        }

        public Task<Container> Get(ulong ownerId, string name)
        {
            return _containerRepository.Get(ownerId, name);
        }

        public Task Update(ulong id, string newName)
        {
            return _containerRepository.Update(new Container
            {
                Id = id,
                Name = newName
            });
        }

        public Task RefreshMapping(ulong ownerId, IEnumerable<ContainerMappingRefreshRequest> mappings)
        {
            throw new NotImplementedException();
        }

        private Task RefreshMapping(IEnumerable<ContainerPropertyMapping> mappings)
        {
            return _containerPropertyMappingRepository.Insert(mappings);
        }
    }
}