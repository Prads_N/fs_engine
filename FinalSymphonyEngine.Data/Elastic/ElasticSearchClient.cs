﻿namespace FinalSymphonyEngine.Data.Elastic
{
    using FinalSymphonyEngine.Models;
    using Models.Contracts.Data;
    using Models.Data;
    using Models.Query;
    using Models.Responses;
    using Nest;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class ElasticSearchClient : IElasticSearchClient
    {
        private readonly IElasticClient _elasticClient;

        public ElasticSearchClient(string endpoint)
        {
            _elasticClient = new ElasticClient(new Uri(endpoint));
        }

        public async Task<QueryResponse> Count(ElasticQuery query)
        {
            var result = await _elasticClient.CountAsync<ElasticDocument>(cd => query.CountDescriptor);

            if (result.IsValid)
                return new QueryResponse { Data = result.Count };

            throw new Exception($"{result.ServerError.Error.Reason}. {result.DebugInformation}", result.OriginalException);
        }

        public async Task<QueryResponse> Extract(ElasticQuery query)
        {
            var result = await _elasticClient.SearchAsync<ElasticDocument>(query.SearchDescriptor);

            if (result.IsValid)
                return new QueryResponse { Data = result.Hits.Select(s => s.Source.DocumentId) };

            throw new Exception($"{result.ServerError.Error.Reason}. {result.DebugInformation}", result.OriginalException);
        }

        public async Task Store(string indexName, ElasticDocument document)
        {
            var result = await _elasticClient.IndexAsync(document, s => s.Index(indexName).Type(nameof(ElasticDocument).ToLower()));

            if (!result.IsValid)
                throw new Exception($"{result.ServerError.Error.Reason}. {result.DebugInformation}", result.OriginalException);
        }

        public Task<ISearchResponse<ElasticDocument>> Scan(ElasticQuery query)
        {
            query.SearchDescriptor
                .Size(Constants.Elasticsearch.MaxDocumentWindow)
                .Scroll("2m");

            return _elasticClient.SearchAsync<ElasticDocument>();
        }

        public Task<ISearchResponse<ElasticDocument>> Scroll(string scrollId)
        {
            return _elasticClient.ScrollAsync<ElasticDocument>("2m", scrollId);
        }
    }
}