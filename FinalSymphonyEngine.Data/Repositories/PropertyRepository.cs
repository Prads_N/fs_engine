﻿namespace FinalSymphonyEngine.Data.Repositories
{
    using System.Threading.Tasks;
    using Repositories.Primitives;
    using Models.Data;
    using Models.Contracts.Data;
    using FinalSymphonyEngine.Models;
    using System.Collections.Generic;

    public class PropertyRepository : BaseRepository, IPropertyRepository
    {
        public PropertyRepository(IDbConnectionFactory dbConnectionFactory) 
            : base(dbConnectionFactory) { }

        public Task<Property> Get(string name)
        {
            return QuerySingleOrDefault<Property>($"SELECT * FROM {Constants.DatabaseTables.Properties} WHERE `Name`=@name;", new { name });
        }

        public Task<IEnumerable<Property>> GetMany(IEnumerable<string> names)
        {
            return Query<Property>($"SELECT * FROM {Constants.DatabaseTables.Properties} WHERE `Name` IN @names;", new { names });
        }

        public Task Insert(string name)
        {
            return Execute($"INSERT IGNORE INTO {Constants.DatabaseTables.Properties} (`Name`) VALUES (@name);", new { name });
        }
    }
}