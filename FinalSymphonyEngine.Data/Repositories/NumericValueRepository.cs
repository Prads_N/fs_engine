﻿namespace FinalSymphonyEngine.Data.Repositories
{
    using FinalSymphonyEngine.Models;
    using FinalSymphonyEngine.Models.Data;
    using Models.Contracts.Data;
    using Primitives;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class NumericValueRepository : BaseRepository, INumericValueRepository
    {
        public NumericValueRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory) { }

        public Task Insert(IEnumerable<NumericValue> numericValue)
        {
            var sql = $@"INSERT INTO {Constants.DatabaseTables.NumericValues} 
                            (`ContainerId`,`DocumentId`,`PropertyId`,`Double`,`SignedInteger`,`UnsignedInteger`) 
                            VALUES 
                            (@ContainerId,@DocumentId,@PropertyId,@Double,@SignedInteger,@UnsignedInteger);";

            return Execute(sql, numericValue);
        }

        public Task<IEnumerable<double>> GetDoubleValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds)
        {
            var sql = $@"SELECT `Double` FROM {Constants.DatabaseTables.NumericValues} 
                            WHERE `ContainerId`=@containerId AND `PropertyId`=@propertyId AND `DocumentId` IN @documentIds;";

            return Query<double>(sql, new { containerId, propertyId, documentIds });
        }

        public Task<IEnumerable<long>> GetSignedIntegerValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds)
        {
            var sql = $@"SELECT `SignedInteger` FROM {Constants.DatabaseTables.NumericValues}
                            WHERE `ContainerId`=@containerId AND `PropertyId`=@propertyId AND `DocumentId` IN @documentIds;";

            return Query<long>(sql, new { containerId, propertyId, documentIds });
        }

        public Task<IEnumerable<ulong>> GetUnsignedIntegerValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds)
        {
            var sql = $@"SELECT `UnsignedInteger` FROM {Constants.DatabaseTables.NumericValues}
                            WHERE `ContainerId`=@containerId AND `PropertyId`=@propertyId AND `DocumentId` IN @documentIds;";

            return Query<ulong>(sql, new { containerId, propertyId, documentIds });
        }
    }
}