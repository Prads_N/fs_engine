﻿namespace FinalSymphonyEngine.Data.Repositories
{
    using Models.Data;
    using Models.Contracts.Data;
    using Primitives;
    using System.Threading.Tasks;
    using Models;
    using System.Collections.Generic;
    using FinalSymphonyEngine.Utils.Extensions;

    public class ContainerRepository : BaseRepository, IContainerRepository
    {
        public ContainerRepository(IDbConnectionFactory dbConnectionFactory) 
            : base(dbConnectionFactory) { }

        public Task Create(Container container)
        {
            return Execute($"INSERT IGNORE INTO {Constants.DatabaseTables.Containers} (`Name`,`OwnerId`,`DateCreated`) VALUES (@Name,@OwnerId,UTC_TIMESTAMP());",
                container);
        }

        public Task<Container> Get(ulong ownerId, string name)
        {
            return QuerySingleOrDefault<Container>($"SELECT * FROM {Constants.DatabaseTables.Containers} WHERE `OwnerId`=@ownerId AND `Name`=@name;",
                new { ownerId, name });
        }

        public Task<Container> Get(ulong ownerId, ulong containerId)
        {
            return QuerySingleOrDefault<Container>($"SELECT * FROM {Constants.DatabaseTables.Containers} WHERE `Id`=@containerId AND `OwnerId`=@ownerId;",
                new { containerId, ownerId });
        }

        public Task<IEnumerable<Container>> GetAll(ulong ownerId)
        {
            return Query<Container>($"SELECT * FROM {Constants.DatabaseTables.Containers} WHERE `OwnerId`=@ownerId;", new { ownerId });
        }

        public Task<IEnumerable<Container>> Get(ulong ownerId, int page, int limit)
        {
            return Query<Container>($"SELECT * FROM {Constants.DatabaseTables.Containers} WHERE `OwnerId`=@ownerId ORDER By Id LIMIT @offset,@limit",
                new { ownerId, limit, offset = page.GetOffset(limit) });
        }

        public Task Update(Container container)
        {
            return Execute($"UPDATE {Constants.DatabaseTables.Containers} SET Name=@Name AND `DateModified`=UTC_TIMESTAMP() WHERE `Id`=@Id;", container);
        }
    }
}