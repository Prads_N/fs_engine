﻿namespace FinalSymphonyEngine.Data.Repositories
{
    using FinalSymphonyEngine.Data.Repositories.Primitives;
    using FinalSymphonyEngine.Models;
    using FinalSymphonyEngine.Models.Contracts.Data;
    using FinalSymphonyEngine.Models.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ContainerPropertyMappingRepository : BaseRepository, IContainerPropertyMappingRepository
    {
        public ContainerPropertyMappingRepository(IDbConnectionFactory dbConnectionFactory) 
            : base(dbConnectionFactory) { }

        public Task Insert(IEnumerable<ContainerPropertyMapping> models)
        {
            var sql = $@"INSERT IGNORE INTO {Constants.DatabaseTables.ContainerPropertyMappings} 
                            (`ContainerId`,`PropertyId`,`ValueDataType`)
                        VALUES
                            (@ContainerId,@PropertyId,@ValueDataType);";

            return Execute(sql, models);
        }

        public Task<IEnumerable<ContainerPropertyMapping>> Get(ulong containerId)
        {
            var sql = $"SELECT * FROM {Constants.DatabaseTables.ContainerPropertyMappings} WHERE `ContainerId`=@containerId;";
            return Query<ContainerPropertyMapping>(sql, new { containerId });
        }
    }
}