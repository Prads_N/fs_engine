﻿namespace FinalSymphonyEngine.Data.Repositories
{
    using FinalSymphonyEngine.Data.Repositories.Primitives;
    using FinalSymphonyEngine.Models;
    using FinalSymphonyEngine.Models.Data;
    using Models.Contracts.Data;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class KeywordValueRepository : BaseRepository, IKeywordValueRepository
    {
        public KeywordValueRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory) { }

        public Task Insert(IEnumerable<KeywordValue> keywordValue)
        {
            var sql = $@"INSERT INTO {Constants.DatabaseTables.KeywordValues} 
                            (`ContainerId`,`DocumentId`,`PropertyId`,`Hash`,`Keyword`)
                            VALUES
                            (@ContainerId,@DocumentId,@PropertyId,@Hash,@Keyword);";

            return Execute(sql, keywordValue);
        }

        public Task<IEnumerable<string>> GetStringValues(ulong containerId, ulong propertyId, IEnumerable<ulong> documentIds)
        {
            var sql = $@"SELECT `Keyword` FROM {Constants.DatabaseTables.KeywordValues} 
                            WHERE `ContainerId`=@containerId AND `PropertyId`=@propertyId AND `DocumentId` IN @documentIds;";

            return Query<string>(sql, new { containerId, propertyId, documentIds });
        }

        public Task<IEnumerable<string>> GetStringValues(string hashValue)
        {
            var sql = $"SELECT `Keyword` FROM {Constants.DatabaseTables.KeywordValues} WHERE `Hash`=@hash;";

            return Query<string>(sql, new { hashValue });
        }
    }
}